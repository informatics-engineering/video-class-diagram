# Class Diagram

## Apa itu Class Diagram ???
- Diagram yang memodelkan struktur statik dari sebuah sistem
- Menggambarkan:
    - Class
    - Attributes: Atribut dari Class tersebut
    - Operations: Aksi yang dapat dilakukan oleh Class tersebut
- Dapat digunakan untuk:
    - Pemodelan kelas pada pemrograman berorientasi objek
    - Pemodelan proses bisnis

## Mengapa Class Diagram digunakan ?
- Mengkomunikasikan struktur statik dari sistem
- Memudahkan perancangan dan analisis struktur statik dari sebuah sistem
- Menunjukkan tanggung jawab dari setiap komponen sistem

## Komponen Class Diagram
1. Class name: Nama Class
2. Class attributes: Atribut Class, tipe data, modifier
3. Class operations: Operasi Class, tipe data output, parameter operasi, modifier

## Modifier
Tanda | Deskripsi
--- | ---
\+ | Modifier public
\- | Modifier private
\# | Modifier protected


## Contoh Pembuatan Class
```mermaid
classDiagram
    class Mahasiswa {
        -String nim
        -String kodeJurusan
        +getNim()
        +getKodeJurusan()
        +daftarUlang(semester, tahun)
        +lihatTranskrip(semester, tahun)
    }
```

## Bentuk Relasi Antar Class
```mermaid
classDiagram
classA --|> classB : Inheritance
classC --* classD : Composition
classE --o classF : Aggregation
classG --> classH : Association
classI -- classJ : Link(Solid)
classK ..> classL : Dependency
classM ..|> classN : Realization
classO .. classP : Link(Dashed)
```

## Contoh Ralasi Antar Class
```mermaid
classDiagram
    class Manusia {
        #String nama
        +getNama()
    }

    class Mahasiswa {
        -String nim
        -String kodeJurusan
        +getNIM()
        +getKodeJurusan()
        +daftarUlang(semester, tahun)
        +lihatTranskrip(semester, tahun)
    }

    class Dosen {
        -String nip
        -String kodeJurusan
        -Mahasiswa List~mahasiswaBimbingan~
        +getNIP()
        +getKodeJurusan()
    }

    Manusia <|-- Mahasiswa : Inheritance
    Manusia <|-- Dosen : Inheritance
    Dosen --> Mahasiswa : Membimbing
```

## Challenge
Menambahkan Class Diagram menggunakan Mermaid.js pada file MD job interview nya

```mermaid
classDiagram
    class MainGmail {
         +pilihanmenu : int
    }

    class User {
        -username : String
        -password : String
        +registrasiAkun()
        +loginAkun()
        +logoutAkun()
        +getUsername()
    }

    class MainMenu {
         +Menupilihan : int
         +tampilkan(String username)
    }

    class PencarianEmail {
         +ditemukan : boolean
         +cariEmail(List<Email> daftarEmail, String keyword)
    } 
    
    class abstract Email{
         #emails : String
         +Email()
         +abstract tampilkan()
         +tambahEmail()
         +hitungJumlahEmail()
         +getEmails()

    }

    class SpamEmail {
         +tampilkan()
         +hitungJumlahEmail()
    }

    class Mengarsipkan {
         +tampilkan()
         +hitungJumlahEmail()
    }

    class Kotak_Masuk {
         +tampilkan()
         +hitungJumlahEmail()
    }

    class Terkirim {
         +Terkirim()
         +tambahStatusPengiriman(String status)
         +tampilkan()
         +tambahEmail()
    }

    class Buat {
         -email: String
         -pengirim: String
         -tujuan: String
         -subjek: String
         -isi: String
         +getEmail()
         +tampilkan()
    }

    class Draf {
         +tampilkan()
         +hitungJumlahEmail()
    }

    class Hapus {
         +tampilkan()
         +hitungJumlahEmail()
    }

    class Meneruskan {
         +tampilkan()
         +hitungJumlahEmail()
    }
    MainGmail -- User 
    User -- MainMenu
    MainMenu -- PencarianEmail
    PencarianEmail -- abstract Email
    MainMenu -- abstract Email
    abstract Email <|-- SpamEmail
    abstract Email <|-- Mengarsipkan
    abstract Email <|-- Kotak_Masuk
    abstract Email <|-- Terkirim
    abstract Email <|-- Buat
    abstract Email <|-- Draf 
    abstract Email <|-- Hapus 
    abstract Email <|-- Meneruskan
```

- Keterangan 
 
    - Setiap kelas miliki hubungan antar kelas, seperti kelas "MainGmail", "User","MainMenu","PencarianEmail","abstract Email" memiliki hubungan asosiasi yaitu sebagai hubungan antara dua class yang bersifat statis.
    - Pada kelas "Kotak_Masuk", "Terkirim", "Buat", "Draf", "abstrack Email", dll. memiliiki hubungan pewarisan yaitu kemampuan untuk mewarisi seluruh atribut dan metode dari kelas induk (superclass) ke anak kelas (subclass).
    - Pada Setiap Kelas bisa terdapat atribut maupun method. Untuk method memiliki ciri dengan menggunakan "()".
    - Simbol "+" = Public, Simbol "-" = Private, Simbol "#" = Protected


### Video Penjelasan Class Diagram
- [Link Youtube](https://www.youtube.com/watch?v=dxq1z5lHReY&feature=share8)

